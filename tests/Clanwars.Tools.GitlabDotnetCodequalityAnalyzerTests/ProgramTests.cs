using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using Clanwars.Tools.BuildOutputAnalyzer;
using Clanwars.Tools.BuildOutputAnalyzer.Classes;
using FluentAssertions;
using Moq;
using Xunit;

namespace Clanwars.Tools.BuildOutputAnalyzerTests;

public class ProgramTests
{
    private readonly StringWriter _fakeOutput = new();
    
    public ProgramTests()
    {
        var pipedInputCheckerMock = new Mock<IPipedInputChecker>();
        pipedInputCheckerMock.Setup(x => x.IsPipedInput()).Returns(true);
        
        PipedInputChecker.Current = pipedInputCheckerMock.Object;
        Console.SetOut(_fakeOutput);  
    }

    [Theory]
    [InlineData(@"C:\Repository\SomeApp\SomeAppNameSpace\Repositories\SomeRepository.cs(63,9): warning CS8602: Dereference of a possibly null reference. [C:\Repository\SomeApp\SomeAppNameSpace\SomeAppNameSpace.csproj]")]
    [InlineData(@"C:\Repository\SomeApp\SomeAppNameSpaceTests\MapsterTests.cs(28,9): warning S125: Remove this commented out code. [C:\Repository\SomeApp\SomeAppNameSpaceTests\SomeAppNameSpaceTests.csproj]")]
    public void CqRegex_WhenCorrectBuildOutputLine_ShouldBeMatched(string line)
    {
        // arrange
        Console.SetIn(new StringReader(line));

        // act
        Program.Main(Array.Empty<string>());

        // assert
        var outputIssues = DeserializeConsoleOutput();
        outputIssues.Should().ContainSingle();
    }
    
    [Theory]
    [InlineData(@"warning CS8602: Dereference of a possibly null reference.")]
    [InlineData(@"C:\Repository\SomeApp\SomeAppNameSpace\Repositories\SomeRepository.cs(63,9)")]
    public void CqRegex_WhenIncorrectBuildOutputLine_ShouldNotBeMatched(string line)
    {
        // arrange
        Console.SetIn(new StringReader(line));

        // act
        Program.Main(Array.Empty<string>());

        // assert
        var outputIssues = DeserializeConsoleOutput();
        outputIssues.Should().BeEmpty();
    }
    
    [Fact]
    public void Main_BuildOutputLine_ShouldBeParsedToCodeQualityIssue()
    {
        // arrange
        Console.SetIn(new StringReader(@"C:\Repository\SomeApp\SomeAppNameSpaceTests\MapsterTests.cs(28,9): warning S125: Remove this commented out code. [C:\Repository\SomeApp\SomeAppNameSpaceTests\SomeAppNameSpaceTests.csproj]"));

        // act
        Program.Main(Array.Empty<string>());

        // assert
        var outputIssues = DeserializeConsoleOutput();
        outputIssues.Should().ContainSingle();
        outputIssues.Single().Type.Should().Be("issue");
        outputIssues.Single().Severity.Should().Be("major");
        outputIssues.Single().Description.Should().Be("Remove this commented out code.");
        outputIssues.Single().Location.Path.Should().Be(@"C:\Repository\SomeApp\SomeAppNameSpaceTests\MapsterTests.cs");
        outputIssues.Single().Location.Lines.Begin.Should().Be(28);
        outputIssues.Single().Fingerprint.Should().NotBeNullOrEmpty();
    }
    
    [Fact]
    public void Main_WhenDifferentBuildOutputLines_AllShouldBeTaken()
    {
        // arrange
        var lines = string.Join(Environment.NewLine,
            @"C:\Repository\SomeApp\SomeAppNameSpace\Repositories\SomeRepository.cs(63,9): warning CS8602: Dereference of a possibly null reference. [C:\Repository\SomeApp\SomeAppNameSpace\SomeAppNameSpace.csproj]",
            @"C:\Repository\SomeApp\SomeAppNameSpaceTests\MapsterTests.cs(28,9): warning S125: Remove this commented out code. [C:\Repository\SomeApp\SomeAppNameSpaceTests\SomeAppNameSpaceTests.csproj]");
        
        Console.SetIn(new StringReader(lines));

        // act
        Program.Main(Array.Empty<string>());

        // assert
        var outputIssues = DeserializeConsoleOutput();
        outputIssues.Should().HaveCount(2);
    }
    
    [Fact]
    public void Main_WhenTwoSameBuildOutputLines_OnlyOneShouldBeTaken()
    {
        // arrange
        var line = @"C:\Repository\SomeApp\SomeAppNameSpace\Repositories\SomeRepository.cs(63,9): warning CS8602: Dereference of a possibly null reference. [C:\Repository\SomeApp\SomeAppNameSpace\SomeAppNameSpace.csproj]";
        var duplicateLines = string.Join(Environment.NewLine, line, line);
        Console.SetIn(new StringReader(duplicateLines));

        // act
        Program.Main(Array.Empty<string>());

        // assert
        var outputIssues = DeserializeConsoleOutput();
        outputIssues.Should().ContainSingle();
    }
    
    [Fact]
    public void Main_WhenBasePathSpecified_ParsedIssuePathShouldBeCut()
    {
        // arrange
        Console.SetIn(new StringReader(@"C:\Repository\SomeApp\SomeAppNameSpaceTests\MapsterTests.cs(28,9): warning S125: Remove this commented out code. [C:\Repository\SomeApp\SomeAppNameSpaceTests\SomeAppNameSpaceTests.csproj]"));

        // act
        Program.Main(new[] { "--base-path", @"C:\Repository\SomeApp\" });

        // assert
        var outputIssues = DeserializeConsoleOutput();
        outputIssues.Should().ContainSingle();
        outputIssues.Single().Location.Path.Should().Be(@"SomeAppNameSpaceTests\MapsterTests.cs");
    }

    [Fact]
    public void Main_WhenNotPipedInput_ReturnsNoResult()
    {
        // arrange
        var pipedInputCheckerMock = new Mock<IPipedInputChecker>();
        pipedInputCheckerMock.Setup(x => x.IsPipedInput()).Returns(false);
        PipedInputChecker.Current = pipedInputCheckerMock.Object;
        
        // act
        Program.Main(Array.Empty<string>());

        // assert
        _fakeOutput.ToString().Should().BeEmpty();
    }

    private ICollection<CodeQualityIssue> DeserializeConsoleOutput()
    {
        var jsonSerializerOptions = new JsonSerializerOptions
        {
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase
        };
        
        return JsonSerializer.Deserialize<ICollection<CodeQualityIssue>>(_fakeOutput.ToString(), jsonSerializerOptions)!;
    }
}
