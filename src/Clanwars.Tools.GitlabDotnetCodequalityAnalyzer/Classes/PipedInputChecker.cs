using System;

namespace Clanwars.Tools.BuildOutputAnalyzer.Classes;

public static class PipedInputChecker
{
    public static IPipedInputChecker Current { get; set; } = new DefaultPipedInputChecker();

    private class DefaultPipedInputChecker : IPipedInputChecker
    {
        public bool IsPipedInput()
        {
            try
            {
                var isKey = Console.KeyAvailable;
                return false;
            }
            catch
            {
                return true;
            }
        }
    }
}
