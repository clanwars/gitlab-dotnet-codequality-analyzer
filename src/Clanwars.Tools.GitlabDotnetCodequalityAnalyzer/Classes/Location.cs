namespace Clanwars.Tools.BuildOutputAnalyzer.Classes;

public class Location
{
    public string Path { get; set; }
    public Lines Lines { get; set; }
}
