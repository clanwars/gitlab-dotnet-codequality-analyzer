namespace Clanwars.Tools.BuildOutputAnalyzer.Classes;

public interface IPipedInputChecker
{
    bool IsPipedInput();
}