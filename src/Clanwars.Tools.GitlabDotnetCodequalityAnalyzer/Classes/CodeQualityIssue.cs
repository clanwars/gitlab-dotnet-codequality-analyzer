namespace Clanwars.Tools.BuildOutputAnalyzer.Classes;

public class CodeQualityIssue
{
    public string Type { get; set; } = "issue";
    public string Severity { get; set; } = "major";
    public string Fingerprint { get; set; }
    public Location Location { get; set; }
    public string Description { get; set; }
}