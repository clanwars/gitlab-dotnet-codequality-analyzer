using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.NamingConventionBinder;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using Clanwars.Tools.BuildOutputAnalyzer.Classes;

namespace Clanwars.Tools.BuildOutputAnalyzer;

public class Program
{
    private static readonly Regex CqRegex = new Regex(@"(?<path>.+\.\w+)\((?<linesBegin>\d+),(\d+)\):\s(\w+)\s(.+):\s(?<desc>.+)\s\[(.+)\]");

    public static int Main(string[] args)
    {
        // Create a root command with some options
        var basePathOption = new Option<string>(
            "--base-path",
            getDefaultValue: () => "",
            description: "Base path which should be stripped out on issue paths.");
        basePathOption.AddAlias("-p");
        var rootCommand = new RootCommand()
        {
            basePathOption
        };

        rootCommand.Description =
            "Analyzes a dotnet build text output on StdIn and generates a GitLab CodeQuality report.";

        // Note that the parameters of the handler method are matched according to the names of the options
        rootCommand.Handler = CommandHandler.Create<string>(RunCommand);

        // Parse the incoming args and invoke the handler
        return rootCommand.InvokeAsync(args).Result;
    }

    private static int RunCommand(string basePath)
    {
        // if nothing is being piped in, then exit
        if (!PipedInputChecker.Current.IsPipedInput())
            return 0;

        var issues = new Dictionary<string, CodeQualityIssue>();

        var jsonSerializerOptions = new JsonSerializerOptions
        {
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase
        };

        using var sha256 = SHA256.Create();

        while (Console.In.Peek() != -1)
        {
            var inputLine = Console.In.ReadLine();
            var match = CqRegex.Match(inputLine!);
            if (!match.Success) continue;

            var strippedPath = match.Groups["path"].Value;
            var index = strippedPath.IndexOf(basePath, StringComparison.Ordinal);
            if (basePath != "" && index >= 0)
            {
                strippedPath = strippedPath.Remove(index, basePath.Length);
            }

            var iss = new CodeQualityIssue
            {
                Description = match.Groups["desc"].Value,
                Location = new Location
                {
                    Path = strippedPath,
                    Lines = new Lines
                    {
                        Begin = Convert.ToInt32(match.Groups["linesBegin"].Value),
                    }
                }
            };

            iss.Fingerprint =
                Convert.ToBase64String(
                    sha256.ComputeHash(
                        Encoding.UTF8.GetBytes(JsonSerializer.Serialize(iss, jsonSerializerOptions))));
            issues.TryAdd(iss.Fingerprint, iss);
        }

        Console.Write(JsonSerializer.Serialize(issues.Values, jsonSerializerOptions));

        return 0;
    }
}
