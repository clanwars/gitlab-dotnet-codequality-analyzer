# Code Quality Helper
> CICD job

Analyzes a dotnet build text output on StdIn and generates a GitLab CodeQuality report.

### Deploying / Publishing

Deploy step is described in .gitlab-ci.yml.
It will generate [gitlab-dotnet-codequality-analyzer container](registry.gitlab.com/clanwars/gitlab-dotnet-codequality-analyzer) using [kaniko](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html)

## Features

* Parse build output.
* Find build errors and warnings.
* Show code quality problems in merge request.

## Configuration

Add GitLab CI/CD job

```yaml
cq_build_outputs:
  stage: test
  image: registry.gitlab.com/clanwars/gitlab-dotnet-codequality-analyzer:latest
  needs:
    - %build-job-name%
  artifacts:
    expire_in: 7 days
    paths:
      - gc-build-analyzer.json
    reports:
      codequality:
        - gc-build-analyzer.json
  script:
    - ls -la ${CI_PROJECT_DIR}/build-stdout/
    - cat ${CI_PROJECT_DIR}/build-stdout/*.txt | dotnet /app/Clanwars.Tools.GitlabDotnetCodequalityAnalyzer.dll > gc-build-analyzer.json
    - cat gc-build-analyzer.json
```

Replace %build-job-name% with job name what will generate build output in /build-stdout/*.txt file

## Links

- Repository: https://gitlab.com/clanwars/gitlab-dotnet-codequality-analyzer
- GitLab CI\CD code quality - https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportscodequality
- Kaniko - https://docs.gitlab.com/ee/ci/docker/using_kaniko.html
- Image registry - registry.gitlab.com/clanwars/gitlab-dotnet-codequality-analyzer
